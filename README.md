<h4 align="center">

![](./site.png)

 <strong>Know more about your pet!</strong> 🐶️
</h4>
<span>
  <a href="http://oowlish.com">
    <img alt="Made by Oowlish" src="https://img.shields.io/badge/made%20by-Oowlish-red">
  </a>
  <img alt="License" src="https://img.shields.io/badge/license-MIT-red">
</span>

<br>

### :rocket: Technologies and plugins used
This wonderful project was developed with the following technologies:
- [Wordpress](https://wordpress.org/)
- [Advanced Custom Fields](https://www.advancedcustomfields.com/)
- [wpDiscuz](https://wordpress.org/plugins/wpdiscuz/)
- [Docker](https://www.docker.com/)
- [Bootstrap 4](https://getbootstrap.com/)
- [SlickJs](https://kenwheeler.github.io/slick/)

### :muscle: Project

<strong>Oowlish Pets</strong> is a project that wants to clarify any doubts about your pet's breed.

### 🦸‍♂️ Getting Started <br>
#### Prerequisites
- [Docker](https://www.docker.com/)
#### Installing
Clone the repository
```
git clone https://gitlab.com/kevynrocha/oowlish-pets.git
```
Enter the folder
```
cd oowlish-pets
```
Run containers
```
docker-compose up -d
```
#### Usage
Access
[http://localhost:8000/](http://localhost:8000/)

Username: wordpress

Password: wordpress
