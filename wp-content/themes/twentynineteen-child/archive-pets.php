<?php
/* 
Template Name: Archive Pets
*/
get_header(); ?>

<div class="archive-pet">
    <div class="container">
        <div class="row">
            <?php 
            $query = new WP_Query(array(
                'post_type'         => 'pets' ,                 
                'orderby'           => 'title',
                'order'             => 'ASC',
                'posts_per_page'    => -1
            )); 

            while ( $query->have_posts() ) : $query->the_post();
                get_template_part( 'template-parts/post', 'pet' );
            endwhile;
            wp_reset_postdata(); ?>
        </div>
    </div>
</div>

<?php get_footer();