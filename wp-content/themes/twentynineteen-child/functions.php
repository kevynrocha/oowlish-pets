<?php

function oowlish_enqueue_styles() {
    // Child theme
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );   

    // Bootstrap
    wp_enqueue_style( 'bootstrap-style' , 'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css' );
    wp_enqueue_script( 'slim-script', 'https://code.jquery.com/jquery-3.4.1.slim.min.js', null, false, true );
    wp_enqueue_script( 'popper-script', 'https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js', null, false, true );
    wp_enqueue_script( 'bootstrap-script', 'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js', null, false, true ); 

    // SlickSlider
    wp_enqueue_style( 'slick', get_theme_file_uri() . '/assets/slick/slick.css' );
    wp_enqueue_style( 'slick-theme', get_theme_file_uri() . '/assets/slick/slick-theme.css' );
    wp_enqueue_script( 'slick-script', get_theme_file_uri() . '/assets/slick/slick.min.js', null, false, true );
}

add_action( 'wp_enqueue_scripts', 'oowlish_enqueue_styles' );

/*
* Creating a CPT
*/
 
function custom_post_type_pets() { 
    
    $labels = array(
        'name'                => _x( 'Pets', 'Post Type General Name'),
        'singular_name'       => _x( 'Pet', 'Post Type Singular Name'),
        'menu_name'           => __( 'Pets'),
        'parent_item_colon'   => __( 'Parent Pet'),
        'all_items'           => __( 'All Pets'),
        'view_item'           => __( 'View Pet'),
        'add_new_item'        => __( 'Add New Pet'),
        'add_new'             => __( 'Add New'),
        'edit_item'           => __( 'Edit Pet'),
        'update_item'         => __( 'Update Pet'),
        'search_items'        => __( 'Search Pet'),
        'not_found'           => __( 'Not Found'),
        'not_found_in_trash'  => __( 'Not found in Trash'),
    );    
        
    $args = array(
        'label'               => __( 'pets'),
        'labels'              => $labels,
        'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'comments', 'revisions', ), 
        'taxonomies'          => array( 'pet' ),            
        'hierarchical'        => true,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'show_in_rest'        => true,  
        'menu_icon'           => 'dashicons-buddicons-activity'
    );        
    
    register_post_type( 'pets', $args );     
}    
add_action( 'init', 'custom_post_type_pets', 0 );

/*
* Creating a taxonomy
*/

function create_pet_category_taxonomy() { 
    $labels = array(
        'name'              => _x( 'Pet Category', 'taxonomy general name' ),
        'singular_name'     => _x( 'Pet', 'taxonomy singular name' ),
        'search_items'      =>  __( 'Search Pet Category' ),
        'all_items'         => __( 'All Pet Category' ),
        'parent_item'       => __( 'Parent Pet' ),
        'parent_item_colon' => __( 'Parent Pet:' ),
        'edit_item'         => __( 'Edit Pet' ), 
        'update_item'       => __( 'Update Pet' ),
        'add_new_item'      => __( 'Add New Pet' ),
        'new_item_name'     => __( 'New Pet Name' ),
        'menu_name'         => __( 'Pet Category' ),
    );    

    register_taxonomy('pet_category',array('pets'), array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_in_rest'      => true,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'pet_category' ),
    ));

}
add_action( 'init', 'create_pet_category_taxonomy', 0 );

/*
* Register Menu
*/

function register_my_menu() {
    register_nav_menus(
      array(
        'header-menu' => __('Header Menu')
      )
    );
}
add_action( 'init', 'register_my_menu' );

/*
* Changing logo and background of the login page
*/

function my_login_logo_one() { ?> 
    <style type="text/css"> 
        body.login div#login h1 a {
            background-image: url(<?php echo get_theme_file_uri(); ?>/assets/images/header-logo.png);
            background-position: bottom;
            background-size: 200px;
            width: unset;
        } 
        
        body.login {
             background-color: #ff5c3e;
        }

        .login #backtoblog a, .login #nav a {
            color: #fff !important;
        }
    </style>
<?php 
} 
add_action( 'login_enqueue_scripts', 'my_login_logo_one' );