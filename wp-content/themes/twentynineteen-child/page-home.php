<?php
/**
 * Template Name: Page Home
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */

get_header(); ?>

<div class="home-pets">
    <section class="home-banner" style="background: url(<?php echo get_the_post_thumbnail_url(); ?>);">
        <div class="container">
            <div class="row">
                <div class="col-6 col-lg-4">
                    <h3><?php the_field('home_banner_title'); ?></h3>
                    <h4><?php the_field('home_banner_subtitle'); ?></h4>
                </div>
            </div>
        </div>    
    </section>

    <section class="container-pet">
        <div class="container">
        <?php $_terms = get_terms( array('pet_category') );
            foreach ($_terms as $term):
                $term_slug = $term->slug;
                $query = new WP_Query( array(
                    'post_type'         => 'pets',
                    'posts_per_page'    => -1,
                    'orderby'           => 'title',
                    'order'             => 'ASC',
                    'tax_query'         => array(
                        array(
                            'taxonomy'  => 'pet_category',
                            'field'     => 'slug',
                            'terms'     => $term_slug,
                        ),
                    ),
                ));

                if( $query->have_posts() ) : ?>

                    <col-12>
                        <h3><?php echo $term->name; ?></h3>
                    </col-12>

                    <div class="row">
                    <?php while ( $query->have_posts() ) : $query->the_post();        
                        get_template_part( 'template-parts/post', 'pet' );        
                    endwhile; ?>
                    </div>

                <?php endif; 
                wp_reset_postdata();
            endforeach; ?>
        </div>
    </section>
</div>

<?php get_footer();
