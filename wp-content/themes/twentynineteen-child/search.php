<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
            <div class="container">
                <div class="row">
                <?php if ( have_posts() ) : ?>

                    <div class="col-12">
                        <header class="page-header">
                            <h1 class="page-title">
                                <?php _e( 'Search results for: ', 'twentynineteen' ); ?>
                                <span class="page-description"><?php echo get_search_query(); ?></span>
                            </h1>
                        </header>
                    </div>

                    <?php
                    while ( have_posts() ) : the_post();				
                        get_template_part( 'template-parts/post', 'pet' ); 				
                    endwhile; ?>
                    
                    <div class="col-12 text-center">
                        <?php twentynineteen_the_posts_navigation(); ?>
                    </div>                   
                    
                    <?php
                    else :
                    get_template_part( 'template-parts/content/content', 'none' );

                    endif;
                    ?>
                </div>
            </div>
		</main>
	</div>

<?php
get_footer();
