<form action="/" method="get" class="form-inline my-2 my-lg-0">
    <input class="form-control" type="search" placeholder="Search your pet..." aria-label="Search your pet..." name="s" id="search" value="<?php the_search_query();?>">
</form>