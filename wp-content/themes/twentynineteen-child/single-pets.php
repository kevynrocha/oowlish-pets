<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */

get_header(); ?>

	<div class="single-pet">
		<section class="pet-content">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-sm-6">
					<?php while ( have_posts() ) : the_post(); ?>
						<h1><?php the_title(); ?></h1>

						<?php if( get_field('height_pet') ): ?>
							<p><strong>Height:</strong> <?php the_field('height_pet'); ?></p>
						<?php endif; ?>

						<?php if( get_field('weight_pet') ): ?>
							<p><strong>Weight:</strong> <?php the_field('weight_pet'); ?></p>
						<?php endif; ?>

						<?php if( get_field('life_expectancy_pet') ): ?>
							<p><strong>Life Expectancy:</strong> <?php the_field('life_expectancy_pet'); ?></p>
						<?php endif; ?>
						
					<?php endwhile; wp_reset_query(); ?>
					</div>
					<div class="col-sm-6 text-center">
						<img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<div class="pet-text">
							<?php the_content(); ?>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="pet-carousel">
			<div class="container">
				<div class="row">
					<div class="col-12 text-center">
						<h3>Other pets</h3>
					</div>
				</div>
				<div class="row js-pets-carousel">
					<?php	
					$pets_terms = wp_get_object_terms( $post->ID,  'pet_category' );
					$pet_slug = $pets_terms[0]->slug;

					$loop = new WP_Query( array(
						'post_type' => 'pets',
						'taxonomy' => 'pet_category',
						'post__not_in' => array( $post->ID ),
						'term' => $pet_slug
					));
					?>

					<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

					<?php get_template_part( 'template-parts/post', 'pet' ); ?>

					<?php endwhile; wp_reset_query(); ?>
				</div>
			</div>
		</section>

		<section class="pet-comments">
			<div class="container">
				<div class="row">
					<div class="col-12">					
						<?php comments_template(); ?>
					</div>
				</div>
			</div>
		</section>
	</div>	

	<?php function petsCarousel() { ?>
    <script>
		$('.js-pets-carousel').slick({
			dots: true,
			infinite: false,
			speed: 300,
			slidesToShow: 4,
			slidesToScroll: 1,
			arrows: true,
			responsive: [
			    {
			      breakpoint: 1025,
			      settings: {
			        slidesToShow: 3
			      }
				},
				{
			      breakpoint: 770,
			      settings: {
			        slidesToShow: 2
			      }
			    },
			    {
			      breakpoint: 480,
			      settings: {
			        slidesToShow: 1
			      }
			    }
		  	]
     	});
	</script>

	<?php } add_action('wp_footer', 'petsCarousel', 100); ?>

<?php get_footer();
