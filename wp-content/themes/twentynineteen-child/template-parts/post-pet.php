<div class="col-md-4 col-xl-3 align-items-stretch mb-4">    
    <article class="box-pet">
        <a href="<?php the_permalink(); ?>">

            <div class="box-pet-body">
                <?php if ( has_post_thumbnail() ): ?>
                    <img src="<?php the_post_thumbnail_url('large'); ?>" alt="<?php the_title(); ?>">
                <?php else: ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/default_image.jpg" alt="<?php the_title(); ?>">
                <?php endif; ?>
            </div>

            <div class="box-pet-footer">
                <h4><?php the_title(); ?></h4>
            </div>
        </a>
    </article>   
</div>
